import datetime
import pendulum
from typing import Dict, List, Set, Tuple

import dateutil.parser
from dateutil.tz import tzlocal
from google.oauth2.credentials import Credentials
from googleapiclient.discovery import build

DAYS = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]


class GoogleCalendarEvents(object):
    def __init__(self, credentials: Credentials) -> None:
        self._credentials = credentials
        self._service = build("calendar", "v3", credentials=self.credentials)
        self._selected_calendars: List[str] = []
        self._available_calendars: Set[str] = set()
        self._all_events: List[Tuple[datetime.datetime, str]] = []
        self.list_calendars()

    @property
    def credentials(self) -> Credentials:
        return self._credentials

    @property
    def selected_calendars(self) -> List[str]:
        return self._selected_calendars

    def select_calendar(self, calendar_id: str) -> None:
        if calendar_id in self._available_calendars:
            self._selected_calendars.append(calendar_id)

    def list_calendars(self, max_result: int = 100) -> List[Tuple[str, str]]:
        """
        Get a list of calendars with id and summary
        Args:
            max_result: Max number of results

        Returns:
            List of pairs. Each pair contains id and summary
        """
        try:
            calendar_results = (
                self._service.calendarList().list(maxResults=max_result).execute()
            )
            calendars = calendar_results.get("items", [])
            calendar_with_id = []
            for calendar in calendars:
                calendar_with_id.append((calendar["id"], calendar["summary"]))
                self._available_calendars.add(calendar["id"])
            return calendar_with_id
        except Exception as exception:
            print(exception)
        return []

    def get_sorted_events(
        self, max_results: int = 10
    ) ->  Dict[str, List[Dict[str, str]]]:
        """
        Events are sorted in time in ascending order
        :param max_results: Max amount of events to return
        :return: List of pairs. Each pair contains date of the event and text
        """
        # TODO: Handle read timeout
        # all_events: Dict[str, dict]
        # all_events: List[Tuple[datetime.datetime, str]] = []
        event_days: Dict[str, dict] = dict()

        # 'Z' indicates UTC time
        pendulum.week_starts_at(pendulum.SUNDAY)
        pendulum.week_ends_at(pendulum.SATURDAY)
        now = pendulum.now()
        week_start = now.start_of("week")
        week_end = now.end_of("week")

        now = pendulum.now().to_iso8601_string()
        # now = datetime.datetime.utcnow().isoformat() + 'Z'
        for calendar_id in self._selected_calendars:
            events = (
                self._service.events()
                .list(
                    calendarId=calendar_id,
                    timeMin=week_start.to_iso8601_string(),
                    timeMax=week_end.to_iso8601_string(),
                    singleEvents=True,
                    orderBy="startTime",
                )
                .execute()
            )
            events = events.get("items", [])
            for event in events:
                start_time = event["start"].get("dateTime", event["start"].get("date"))
                end_time = event["end"].get("dateTime", event["end"].get("date"))
                summary = event["summary"]
                parsed_start = pendulum.parse(start_time)
                parsed_end = pendulum.parse(end_time)
                day_of_week = DAYS[parsed_start.day_of_week]
                if day_of_week not in event_days:
                    event_days[day_of_week] = []
                event_days[day_of_week].append(
                    {
                        "start_time": parsed_start.to_iso8601_string(),
                        "end_time": parsed_end.to_iso8601_string(),
                        "description": summary,
                    }
                )
        return event_days
