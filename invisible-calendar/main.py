#!/usr/bin/env python3
# -*- coding=utf-8 -*-
import time

import pendulum


try:
    from .calendar_widget import CalendarGrid
    from .configuration import load_or_create_config
    from .events import GoogleCalendarEvents
except ImportError:
    from calendar_widget import CalendarGrid, DAYS
    from configuration import load_or_create_config
    from events import GoogleCalendarEvents
    from weather import WeatherModel


_config = load_or_create_config("./.keys")


def _draw_calendar(cal, calendar_manager):
    for calendar_id in _config.selected_calendars:
        calendar_manager.select_calendar(calendar_id)
    events = calendar_manager.get_sorted_events()

    for day, schedule in events.items():
        for event in schedule:
            start_time, end_time = pendulum.parse(event["start_time"]), pendulum.parse(
                event["end_time"]
            )
            if start_time.hour == end_time.hour:
                end_time = end_time.add(hours=1)
            cal._add_event(
                DAYS.index(day), start_time, end_time, description=event["description"]
            )
    cal.display()


def main():
    cal = CalendarGrid(800, 480, 8, 20)
    calendar_manager = GoogleCalendarEvents(_config.google_credentials)
    weather_manager = WeatherModel(_config.owm_token, _config.city_id)

    cal.setup()
    while True:
        try:
            _draw_calendar(cal, calendar_manager)
            time.sleep(60)
        except KeyboardInterrupt as kerr:
            return


if __name__ == "__main__":
    main()
