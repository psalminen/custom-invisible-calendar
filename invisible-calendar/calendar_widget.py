#!/usr/bin/env python3
# -*- coding=utf-8 -*-
"""
"""

import os
from dataclasses import dataclass

import pendulum
from PIL import Image, ImageDraw, ImageFont

DAYS = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]


@dataclass
class Font:
    event: ImageFont.FreeTypeFont
    hour: ImageFont.FreeTypeFont
    day: ImageFont.FreeTypeFont


@dataclass
class Pad:
    boundary: int
    event: int


@dataclass
class Width:
    total: int
    header: int
    week: int
    day: int


@dataclass
class Height:
    total: int
    header: int
    day: int
    hour: int


class CalendarGrid:
    def __init__(self, width: int, height: int, start_time: int, end_time: int):
        self.start_time = start_time
        self.end_time = end_time

        self._image = Image.new(mode="L", size=(width, height), color=255)

        # TODO CHANGEME
        font_path = os.path.join(
            "/home/psalmin/EInk-Calendar/resources", "Inconsolata-Regular.ttf"
        )
        self._fonts = Font(
            event=ImageFont.truetype(font_path, size=14),
            hour=ImageFont.truetype(font_path, size=10),
            day=ImageFont.truetype(font_path, size=18),  # 24
        )

        self._pads = Pad(
            boundary=5,
            event=2,
        )
        # hour_column=(2 * 2) + self._fonts.hour.getsize(" ")[0],
        header_width = self._pads.boundary * 2 + self._fonts.hour.getsize("  ")[0]
        self._width = Width(
            total=width,
            header=header_width,
            week=width - header_width,
            day=(width - header_width) // 7,
        )

        header_height = self._fonts.day.getsize(" ")[1]
        self._height = Height(
            total=height,
            header=header_height,
            day=height - header_height,
            hour=(height - header_height) // (self.end_time - self.start_time),
        )

    def _write_headers(self):
        draw = ImageDraw.Draw(self._image)
        top = self._pads.boundary
        x_ranges = list(range(self._width.header, self._width.total, self._width.day))
        for i, day in enumerate(DAYS):
            center = x_ranges[i + 1] - (self._width.day // 2)
            day_abbr = day[:3]
            draw.text(
                (center, top), day_abbr, font=self._fonts.day, fill=0, anchor="mt"
            )

        del draw

    def _draw_days(self):
        draw = ImageDraw.Draw(self._image)
        y_start = 0
        y_end = self._height.day

        for x in range(
            self._width.day + self._width.header, self._width.week, self._width.day
        ):
            line = ((x, y_start), (x, y_end))
            draw.line(line, fill=128)

        del draw

    def _draw_hours(self):
        draw = ImageDraw.Draw(self._image)

        # for y in range(0, self._height, step_size):
        for i, y in enumerate(
            range(
                self._height.header + self._pads.boundary,
                self._height.day,
                self._height.hour,
            )
        ):
            if i == 0:
                continue
            hour = i + self.start_time
            if hour > 12:
                hour -= 12
            draw.text(
                (self._width.header - self._pads.boundary, y),
                str(hour),
                font=self._fonts.hour,
                fill=0,
                anchor="rm",
            )
            line = (
                (self._width.header, y),
                (self._width.week + self._pads.boundary, y),
            )
            draw.line(line, fill=128)

        del draw

    def _add_event(
        self,
        day: int,
        start: pendulum.DateTime,
        end: pendulum.DateTime,
        description: str,
    ):
        draw = ImageDraw.Draw(self._image)

        x_ranges = list(
            range(
                self._width.header,
                self._width.total - self._pads.event,
                self._width.day,
            )
        )
        x_start, x_stop = (
            x_ranges[day] + self._pads.event,
            x_ranges[day + 1] - self._pads.event,
        )
        y_ranges = list(
            range(
                self._height.header + self._pads.boundary,
                self._height.day,
                self._height.hour,
            )
        )
        try:
            start_pos = start.hour - self.start_time
            end_pos = end.hour - self.start_time
            if start_pos < 0:
                start_pos = 0
                end_pos = 1
            if end_pos < 0:
                end_pos = 0
            y_start = y_ranges[start_pos] + self._pads.event
            y_stop = y_ranges[end_pos] - self._pads.event
        except:
            return

        if end < pendulum.now():
            fill = 200
            label = False
        else:
            label = True
            fill = 0

        draw.rounded_rectangle((x_start, y_start, x_stop, y_stop), fill=fill)
        if label:
            # size = None
            # font_size = 100
            # while (size is None or size[0] > x_stop - x_start or size[1] > y_stop - y_start) and font_size > 0:
            # while size is None or size[1] > self.hour_step:
            #     font = ImageFont.truetype(font_path, font_size)
            #     size = font.getsize_multiline(description)
            #     font_size -= 1
            if len(description) > 13:
                description = description[:12] + "..."
            # x_center = x_start + (self.day_step // 2)
            y_center = y_start + (self._height.hour // 2)
            draw.text(
                (x_start + self._pads.boundary, y_center),
                description,
                font=self._fonts.event,
                fill=255,
                anchor="lm",
            )

    def setup(self):
        self._write_headers()
        self._draw_days()
        self._draw_hours()
        # self._add_event(2, 10, 12)
        # self._add_event(3, 8, 9)
        # self._image.show()

    def display(self):
        self._image.show()


if __name__ == "__main__":
    print("This is not the py you seek")
