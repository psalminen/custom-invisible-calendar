#!/usr/bin/env python3
# -*- coding=utf-8 -*- 
"""
"""

import os

import pendulum
from pyowm import OWM
from pyowm.weatherapi25.weather import Weather

class WeatherModel:
    def __init__(self, key, city_id):
        self._key = key
        self._city_id = city_id

    def weather_parser(self):
        owm = OWM(self._key)
        mgr = owm.weather_manager()
        forecast = mgr.forecast_at_id(self._city_id, interval='3h')
        weathers = forecast.forecast.weathers
        now = pendulum.now().in_timezone('America/Denver')
        todays_weather = []
        for weather in weathers:
            local_time = pendulum.from_timestamp(weather.ref_time).in_timezone('America/Denver')
            if local_time.day == now.day:
                weather.ref_time = local_time
                todays_weather.append(weather)
        return todays_weather

if __name__ == "__main__":
    print("This is not the py you seek")

